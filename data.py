import cv2
import glob
import numpy as np
import platform

'''
x: Input data. Numpy array of rank 4 or a tuple.
    If tuple, the first element
    should contain the images and the second element
    another numpy array or a list of numpy arrays
    that gets passed to the output
    without any modifications.
    Can be used to feed the model miscellaneous data
    along with the images.
    In case of grayscale data, the channels axis of the image array
    should have value 1, in case
    of RGB data, it should have value 3, and in case
    of RGBA data, it should have value 4.
'''

def load_data(train = 'images/train/', test  = 'images/validation/'):
	if platform.system() == 'Windows':
		sym  = '\\'
	else:
		sym  = '/'

	train_names = glob.glob(train + 'images/input/*.png')
	test_names = glob.glob(test + 'images/input/*.png')

	# print(train_names)
	lX = len(train_names)
	ly = len(test_names)
	train_set = (np.empty(shape = (lX, 1, 128, 128)), np.empty(shape = (lX, 128, 128)))
	test_set = (np.empty(shape = (ly, 1, 128, 128)), np.empty(shape = (ly, 128, 128)))

	# for f in train_names:
	for i in range(1):
		f = train_names[i]
		i1 = cv2.resize(cv2.imread(f, 0), (128, 128))
		i2 = cv2.resize(cv2.imread(train+'masks/input/'+f.split(sym)[-1], 0), (128, 128))
		train_set[0][i, 0] = i1
		train_set[1][i] = i2

	# for f in test_names:
	for i in range(1):
		f = test_names[i]
		i1 = cv2.resize(cv2.imread(f, 0), (128, 128))
		i2 = cv2.resize(cv2.imread(test+'masks/input/'+f.split(sym)[-1], 0), (128, 128))
		test_set[0][i, 0] = i1
		test_set[1][i] = i2

    # if K.image_data_format() == 'channels_last':
	# train_set[0] = train_set[0].transpose(0, 2, 3, 1)
	# test_set[1] = train_set[0].transpose(0, 2, 3, 1)

	# test_set[0] = np.asarray(train_set[0])
	# test_set[1] = np.asarray(train_set[1])
	
	return train_set, test_set
	# 

if __name__ == '__main__':
	load_data()